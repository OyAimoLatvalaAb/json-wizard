pipeline {
    agent {
        label 'nodejs'
    }
    environment {
        NODEJS_HOME = '/opt/tools/nodejs/node-v11.4.0-linux-x64'
        PATH = "/opt/tools/yarn/yarn-v1.12.3/bin:/opt/tools/nodejs/node-v11.4.0-linux-x64/bin:$PATH"
    }
    stages {
        stage('Install Dependencies') {
            steps {
                sh 'yarn install'
            }
        }
        stage('Run tests') {
            steps {
                sh 'yarn test'
            }
        }
        stage('Static code analysis') {
            steps {
                withSonarQubeEnv('VonLatvala Sonar') {
                    script {
                        scannerHome = tool 'SonarScanner 3.2.0.1227'
                    }
                    sh "${scannerHome}/bin/sonar-scanner" +\
                        " -Dsonar.branch.name=\"${env.BRANCH_NAME}\""
                }
            }
        }
        stage("Quality Gate") {
            steps {
                timeout(time: 10, unit: 'MINUTES') {
                    // Parameter indicates whether to set pipeline to UNSTABLE if Quality Gate fails
                    // true = set pipeline to UNSTABLE, false = don't
                    // Requires SonarQube Scanner for Jenkins 2.7+
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Linter') {
            steps {
                sh 'yarn lint'
            }
        }
        stage('Build Project') {
            steps {
                sh 'yarn package'
            }
        }
        stage('Publish Artifacts') {
            when {
                expression { GIT_BRANCH == 'master' }
            }
            steps {
                script {
                    rtUpload (
                        serverId: 'VonLatvala Artifactory',
                        specPath: 'artifactFilespec.json',
                        failNoOp: true
                    )
                    rtPublishBuildInfo (
                        serverId: 'VonLatvala Artifactory',
                    )
                }
            }
        }
    }
}
