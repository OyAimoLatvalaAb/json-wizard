import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import {
    CssBaseline,
    AppBar,
    Toolbar,
    Paper,
    Button,
    Typography,
} from '@material-ui/core'
import InputForm from './components/InputForm';
import JsonOutput from './components/JsonOutput';
import { SaveAlt, DeleteSweep, Create, Search } from '@material-ui/icons';
import NotificationSnackbar from './components/NotificationSnackbar'
import { CATALOGVISIBILITY, MAX_JSON_LENGTH } from './constants/'

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: `${theme.spacing(3)}px 0 ${theme.spacing(5)}px`,
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(),
    },
    leftIcon: {
        marginRight: theme.spacing(),
    },
    rightIcon: {
        marginLeft: theme.spacing(),
    },
});


class App extends React.Component {
    state = {
        salePrice: '',
        dimWCM: '',
        dimLCM: '',
        dimHCM: '',
        manufacturerWebUrl: '',
        unit: '',
        massG: '',
        prodText: '',
        catalogVisibility: CATALOGVISIBILITY.visible,
        generatedJson: '{}',
        notificationIsOpen: false,
        notificationMessage: '',
        snackbarTimer: null
    };

    static propTypes = {
        classes: PropTypes.object.isRequired,
    };

    dataPropagator = (k, v) => {
        const newState = {...this.state}
        newState[k] = v;
        this.setState(newState)
    }

    handleReset = () => {
        this.setState({
            activeStep: 0,
        });
    };

    generateJson = () => {
        const jsonStr = JSON.stringify({
            sale_price: parseFloat((this.state.salePrice+'').replace(',', '.')),
            dim_w_cm: parseFloat((this.state.dimWCM+'').replace(',', '.')),
            dim_l_cm: parseFloat((this.state.dimLCM+'').replace(',', '.')),
            dim_h_cm: parseFloat((this.state.dimHCM+'').replace(',', '.')),
            manufacturer_homepage_url: (this.state.manufacturerWebUrl+'').trim(),
            unit: (this.state.unit+'').trim(),
            mass_g: parseFloat((this.state.massG+'').replace(',', '.')),
            text: (this.state.prodText+'').replace(/\n/g, '<br/>').trim(),
            cat_vis: this.state.catalogVisibility,
        });
        const jsonLength = jsonStr.length;
        if(jsonLength > MAX_JSON_LENGTH) {
            if(this.state.snackbarTimer !== null)
                window.clearTimeout(this.state.snackbarTimer)
            this.setState({
                ...this.state,
                notificationIsOpen: true,
                notificationMessage: `Tuotteen Kuvaus liian pitkä! (+${jsonLength - MAX_JSON_LENGTH})`,
                snackbarTimer: window.setTimeout(() => {
                    this.setState({...this.state, notificationIsOpen: false})
                }, 10000),
                generatedJson: "",
            })
        } else {
            if(this.state.snackbarTimer !== null)
                window.clearTimeout(this.state.snackbarTimer)
            this.setState({...this.state,
                notificationIsOpen: false,
                notificationMessage: '',
                snackbarTimer: null,
                generatedJson: jsonStr});
        }
    }

    clearInputForm = () => {
        this.setState({
            ...this.state,
            salePrice: '',
            dimWCM: '',
            dimLCM: '',
            dimHCM: '',
            manufacturerWebUrl: '',
            unit: '',
            massG: '',
            prodText: '',
            catalogVisibility: CATALOGVISIBILITY.visible,
            generatedJson: '{}'
        })
    }

    onCopy = () => {
        navigator.permissions.query({
            name: 'clipboard-write'
        }).then((permissionStatus) => {
            if(permissionStatus.state === 'granted') {
                console.log('Setting clipboard to:', this.state.generatedJson)
                navigator.clipboard
                    .writeText(this.state.generatedJson)
                    .then((res) => {
                        console.log(res)
                        if(this.state.snackbarTimer !== null)
                            window.clearTimeout(this.state.snackbarTimer)
                        this.setState({
                            ...this.state,
                            notificationIsOpen: true,
                            notificationMessage: 'JSON kopioitu leikepöydälle',
                            snackbarTimer: window.setTimeout(() => {
                                this.setState({...this.state, notificationIsOpen: false})
                            }, 2000)
                        })
                    }).catch((err) => {
                        console.error(err)
                        if(this.state.snackbarTimer !== null)
                            window.clearTimeout(this.state.snackbarTimer)
                        this.setState({
                            ...this.state,
                            notificationIsOpen: true,
                            notificationMessage: 'VIKA: Leikepöydälle ei pystytty kirjoittamaan',
                            snackbarTimer: window.setTimeout(() => {
                                this.setState({...this.state, notificationIsOpen: false})
                            }, 2000)
                        })
                    })
            } else if(permissionStatus.state === 'prompt') {
                console.log('Need to prompt user')
                if(this.state.snackbarTimer !== null)
                    window.clearTimeout(this.state.snackbarTimer)
                this.setState({
                    ...this.state,
                    notificationIsOpen: true,
                    notificationMessage: 'Hyväksy leikepöydän käyttö',
                    snackbarTimer: window.setTimeout(() => {
                        this.setState({...this.state, notificationIsOpen: false})
                    }, 2000)
                })
            }
            else if(permissionStatus.state === 'denied') {
                console.log('Permission to clipboard-write denied')
                if(this.state.snackbarTimer !== null)
                    window.clearTimeout(this.state.snackbarTimer)
                this.setState({
                    ...this.state,
                    notificationIsOpen: true,
                    notificationMessage: 'VIKA: Leikepöydän käyttö estetty',
                    snackbarTimer: window.setTimeout(() => {
                        this.setState({...this.state, notificationIsOpen: false})
                    }, 2000)
                })
            } else {
                console.log('Unknown permission state for clipboard-wrtie')
                if(this.state.snackbarTimer !== null)
                    window.clearTimeout(this.state.snackbarTimer)
                this.setState({
                    ...this.state,
                    notificationIsOpen: true,
                    notificationMessage: 'VIKA: Tuntematon tila leikepödälle kirjoittamiselle',
                    snackbarTimer: window.setTimeout(() => {
                        this.setState({...this.state, notificationIsOpen: false})
                    }, 2000)
                })
            }
        })
    }

    validateJsonDataObj = (jsonDataObj)  => {
        if('sale_price' in jsonDataObj &&
            'dim_w_cm' in jsonDataObj &&
            'dim_l_cm' in jsonDataObj &&
            'dim_h_cm' in jsonDataObj &&
            'manufacturer_homepage_url' in jsonDataObj &&
            'unit' in jsonDataObj &&
            'text' in jsonDataObj &&
            'mass_g' in jsonDataObj &&
            'cat_vis' in jsonDataObj) {
            return true;
        }
        return false;
    }

    convertJsonDataObj = (jsonDataObj) => {
        return {
            dimHCM: jsonDataObj.dim_h_cm,
            dimLCM: jsonDataObj.dim_l_cm,
            dimWCM: jsonDataObj.dim_w_cm,
            manufacturerWebUrl: jsonDataObj.manufacturer_homepage_url,
            massG: jsonDataObj.mass_g,
            prodText: jsonDataObj.text.replace(/<br\/>/g, "\n"),
            salePrice: jsonDataObj.sale_price,
            unit: jsonDataObj.unit,
            catVis: jsonDataObj.cat_vis
        }
    }

    onReadClip = () => {
        navigator.permissions.query({
            name: 'clipboard-read'
        }).then((permissionStatus) => {
            if(permissionStatus.state === 'granted' || permissionStatus.state === 'prompt') {
                navigator.clipboard
                    .readText()
                    .then((res) => {
                        console.log('Clipboard contents:\'', res)
                        let jsonData = {}
                        try {
                            jsonData = JSON.parse(res)
                            console.log(jsonData)
                            if(this.validateJsonDataObj(jsonData)) {
                                const dataObj = this.convertJsonDataObj(jsonData)
                                console.log('Setting state to: ', {...this.state, ...dataObj})
                                if(this.state.snackbarTimer !== null)
                                    window.clearTimeout(this.state.snackbarTimer)
                                this.setState({
                                    ...this.state,
                                    notificationIsOpen: true,
                                    notificationMessage: 'Leikepöytä luettu',
                                    snackbarTimer: window.setTimeout(() => {
                                        this.setState({...this.state, notificationIsOpen: false})
                                    }, 2000),
                                    ...dataObj
                                })
                                this.generateJson()
                            } else {
                                if(this.state.snackbarTimer !== null)
                                    window.clearTimeout(this.state.snackbarTimer)
                                this.setState({
                                    ...this.state,
                                    notificationIsOpen: true,
                                    notificationMessage: 'Leikepöydällä on vääränlaista dataa',
                                    snackbarTimer: window.setTimeout(() => {
                                        this.setState({...this.state, notificationIsOpen: false})
                                    }, 2000),
                                })
                                console.log('Errornous data')
                            }
                        } catch(err) {
                            console.error('Couldn\'t parse JSON from clipboard:', err)
                            if(this.state.snackbarTimer !== null)
                                window.clearTimeout(this.state.snackbarTimer)
                            this.setState({
                                ...this.state,
                                notificationIsOpen: true,
                                notificationMessage: 'VIKA: Leikepöydällä on vääränlaista dataa',
                                snackbarTimer: window.setTimeout(() => {
                                    this.setState({...this.state, notificationIsOpen: false})
                                }, 2000),
                            })
                        }
                    }).catch((err) => {
                        if(this.state.snackbarTimer !== null)
                            window.clearTimeout(this.state.snackbarTimer)
                        this.setState({
                            ...this.state,
                            notificationIsOpen: true,
                            notificationMessage: 'VIKA: Leikepöytää ei pystytty lukemaan',
                            snackbarTimer: window.setTimeout(() => {
                                this.setState({...this.state, notificationIsOpen: false})
                            }, 2000),
                        })
                        console.error(err)
                    })
            }
            else if(permissionStatus.state === 'denied') {
                console.log('Permission to clipboard-read denied')
                if(this.state.snackbarTimer !== null)
                    window.clearTimeout(this.state.snackbarTimer)
                this.setState({
                    ...this.state,
                    notificationIsOpen: true,
                    notificationMessage: 'VIKA: Leikepöydän käyttö estetty',
                    snackbarTimer: window.setTimeout(() => {
                        this.setState({...this.state, notificationIsOpen: false})
                    }, 2000)
                })
            } else {
                console.log('Unknown permission state for clipboard-read')
                if(this.state.snackbarTimer !== null)
                    window.clearTimeout(this.state.snackbarTimer)
                this.setState({
                    ...this.state,
                    notificationIsOpen: true,
                    notificationMessage: 'VIKA: Tuntematon tila leikepödän lukemiselle',
                    snackbarTimer: window.setTimeout(() => {
                        this.setState({...this.state, notificationIsOpen: false})
                    }, 2000)
                })
            }
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar position="absolute" color="default" className={classes.appBar}>
                    <Toolbar>
                        <Typography variant="h6" color="inherit" noWrap>
                            WebShop JSON
                        </Typography>
                    </Toolbar>
                </AppBar>
                <main className={classes.layout}>
                    <NotificationSnackbar
                        open={this.state.notificationIsOpen}
                        message={this.state.notificationMessage}
                    />
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h4" align="center">
                            WebShop JSON Velho
                        </Typography>
                        <React.Fragment>
                            <React.Fragment>
                                <InputForm
                                    propagateData={this.dataPropagator}
                                    salePrice={this.state.salePrice}
                                    dimWCM={this.state.dimWCM}
                                    dimLCM={this.state.dimLCM}
                                    dimHCM={this.state.dimHCM}
                                    manufacturerWebUrl={this.state.manufacturerWebUrl}
                                    unit={this.state.unit}
                                    massG={this.state.massG}
                                    prodText={this.state.prodText}
                                    catalogVisibility={this.state.catalogVisibility}
                                />
                                <div className={classes.buttons}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={this.clearInputForm}
                                        className={classes.button}
                                    >
                                        Tyhjennä
                                        <DeleteSweep
                                            className={classes.rightIcon}
                                        />
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={this.generateJson}
                                        className={classes.button}
                                    >
                                        Luo
                                        <Create
                                            className={classes.rightIcon}
                                        />
                                    </Button>
                                </div>
                                <div className={classes.buttons}>
                                    <Button
                                        variant="contained"
                                        color="secondary"
                                        onClick={this.onReadClip}
                                        className={classes.button}
                                    >
                                        Lue Leikepöydältä
                                        <Search
                                            className={classes.rightIcon}
                                        />
                                    </Button>
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        onClick={this.onCopy}
                                        className={classes.button}
                                    >
                                        Kopioi
                                        <SaveAlt
                                            className={classes.rightIcon}
                                        />
                                    </Button>
                                </div>
                                <JsonOutput
                                    jsonStr={this.state.generatedJson}
                                />
                            </React.Fragment>
                        </React.Fragment>
                    </Paper>
                </main>
            </React.Fragment>
        );
    }
}

export default withStyles(styles)(App);
