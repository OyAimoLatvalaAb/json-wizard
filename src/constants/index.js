export const CATALOGVISIBILITY = {
    visible: 'visible',
    catalog: 'catalog',
    search: 'search',
    hidden: 'hidden'
};
export const MAX_JSON_LENGTH = 4096;
