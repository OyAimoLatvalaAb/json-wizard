import React from 'react';
import { Snackbar } from '@material-ui/core'
import PropTypes from 'prop-types'

class NotificationSnackbar extends React.Component {

    static propTypes = {
        open: PropTypes.bool,
        message: PropTypes.string
    }

    render() {
        return (
            <React.Fragment>
                <Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'right'}}
                    open={this.props.open}
                    message={this.props.message}
                />
            </React.Fragment>
        )
    }
}

export default NotificationSnackbar
