import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import FormHelperText from '@material-ui/core/FormHelperText';
import PropTypes from 'prop-types';
import { CATALOGVISIBILITY } from '../constants/';

class InputForm extends React.Component {
    static propTypes = {
        salePrice: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimWCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimHCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        dimLCM: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string
        ]),
        manufacturerWebUrl: PropTypes.string,
        unit: PropTypes.string,
        massG:  PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ]),
        prodText: PropTypes.string,
        catalogVisibility: PropTypes.oneOf(
            Object.getOwnPropertyNames(CATALOGVISIBILITY).map(
                (k) => CATALOGVISIBILITY[k]
            )
        ),
        propagateData: PropTypes.func.isRequired
    }
    onChange = (e) => {
        this.props.propagateData(e.target.id ? e.target.id : e.target.name, e.target.value)
    }
    render() {
        return (
            <React.Fragment>
                <Typography variant="h6" gutterBottom>
                    Tuotteen Tiedot
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="salePrice"
                            name="salePrice"
                            label="Ale-hinta"
                            fullWidth
                            value={this.props.salePrice}
                            onChange={this.onChange}
                        />
                        <FormHelperText>Jätä tyhjäksi, jollei ole alennusta</FormHelperText>
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimWCM"
                            name="dimWCM"
                            label="Leveys (cm)"
                            fullWidth
                            value={this.props.dimWCM}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimLCM"
                            name="dimLCM"
                            label="Pituus (cm)"
                            fullWidth
                            value={this.props.dimLCM}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={4}>
                        <TextField
                            required
                            id="dimHCM"
                            name="dimHCM"
                            label="Korkeus (cm)"
                            fullWidth
                            value={this.props.dimHCM}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="manufacturerWebUrl"
                            name="manufacturerWebUrl"
                            label="Tuotteen Web URL"
                            fullWidth
                            value={this.props.manufacturerWebUrl}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="unit"
                            name="unit"
                            label="Yksikkö"
                            fullWidth
                            value={this.props.unit}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="massG"
                            name="massG"
                            label="Paino (g)"
                            fullWidth
                            value={this.props.massG}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            required
                            id="prodText"
                            name="prodText"
                            label="Tuotteen Kuvaus"
                            fullWidth
                            multiline
                            rows={4}
                            value={this.props.prodText}
                            onChange={this.onChange}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <InputLabel htmlFor="inputCatVis">Näkyvyys</InputLabel>
                        <Select
                            id="catalogVisibility"
                            name="catalogVisibility"
                            input={<Input name="inputCatVis" id="inputCatVis" />}
                            fullWidth
                            value={this.props.catalogVisibility}
                            onChange={this.onChange}
                        >
                            { Object.getOwnPropertyNames(CATALOGVISIBILITY)
                                .map((k) => CATALOGVISIBILITY[k])
                                .map((val) => (<MenuItem key={'catVis'+val} value={val}>{val}</MenuItem>)) }
                        </Select>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

export default InputForm;
