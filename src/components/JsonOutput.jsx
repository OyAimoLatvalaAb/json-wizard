import React from 'react';
import { TextField, Typography } from '@material-ui/core';
import PropTypes from 'prop-types'

class JsonOutput extends React.Component {
    static propTypes = {
        jsonStr: PropTypes.string
    }
    static styles = theme => ({
        leftIcon: {
            marginRight: theme.spacing(),
        },
    })
    onChange = (e) => {
        e.preventDefault();
    }
    render() {
        return (
            <React.Fragment>
                <Typography variant="h6" gutterBottom
                    style={{marginTop: '20px' }}>
                    JSON Tulos
                </Typography>
                <TextField
                    required
                    id="jsonOutput"
                    name="jsonOutput"
                    label="JSON"
                    fullWidth
                    multiline
                    value={this.props.jsonStr}
                    onChange={this.onChange}
                />
            </React.Fragment>
        )
    }
}

export default JsonOutput;
