# WebShop JSON

UI for formatting JSON data to be fed into Kassa.fi `nimike.webselite`.

## How do I get set up?

To start, you need to have `yarn` installed. This can be done according to [yarn installation guide](https://yarnpkg.com/lang/en/docs/install). When this is done, install the dependencies needed for developing this project by running `yarn install`. After this, you should be able to run `yarn start`, and navigage to [the dev server running on port 3000](http://localhost:3000).
In case you are running your browser from a different computer than you are running `yarn` from, please use the IP or hostname of the computer running yarn instead of localhost. But if you are doing this, you probably understand what this guide is trying to teach you on beforehand.

When making pull requests, [Jenkins](https://jenkins.esovellus.fi) will automatically check it out.

## Author

Oy Aimo Latvala Ab / Axel Latvala
